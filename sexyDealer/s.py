import random
import re
import math

cards = []


class SexyDealer:
    leftAmount = 0
    userList = []
    lastPlayerId = None
    isTrue = True
    passAmount = 0
    nextId = None
    card = []

    def __init__(self, playerAmount):
        self.leftAmount = playerAmount
        self.generate_cards()

    def generate_cards(self):
        textArr = ['A', 'B', 'C', 'D']
        END = 13

        for j in range(len(textArr)):
            for i in range(1, END + 1):
                cards.append(textArr[j] + str(i))
        cards.append('X1')
        cards.append('X2')
        self.showCards()

    def shuffle(self):
        random_end = len(cards)
        while random_end > 0:
            index = random.randint(0, random_end)
            temp = cards[-1]
            cards[-1] = cards[index]
            cards[index] = temp
            random_end = random_end - 1

    def generate_next_id(self, pid):
        status = True
        i = 0
        while status is True and i < len(self.userList):
            if pid == self.userList[i].id:
                status = False
                if i + 1 >= len(self.userList):
                    self.nextId = self.userList[0].id
                else:
                    self.nextId = self.userList[i + 1].id
            else:
                i += 1

    def add_user(self, player):
        self.userList.append(player)

    def handle_play(self, pid, fact, statement):
        # print(cards)
        user_name = ""
        count = len(fact)
        status = False
        fact = str(fact).strip('[]').split(',')
        for i in range(len(fact)):
            fact[i] = fact[i].strip().strip("''")
        for i in range(len(self.userList)):
            if pid == self.userList[i].id:
                status = True
                user_name = self.userList[i].name
        self.isTrue = True
        if status is True:
            self.lastPlayerId = pid
            while len(fact) > 0:
                temp = fact.pop()
                for i in self.userList:
                    if i.id == pid:
                        i.cards.remove(str(temp))
                cards.append(temp)
                if temp != 'X1' and temp != "X2" \
                        and temp.replace(re.match('\D+', temp).group(), '') != statement:
                    self.isTrue = False
            self.generate_next_id(pid)
            # for i in range(len(self.userList)):
            #     if pid == self.userList[i].id:
            #         for j in range(len(self.userList[i].cards)):
            #             for k in range(len(fact)):
            #                 if fact[k] == self.userList[i].cards[j]:
            #                     self.userList[i].cards.pop(j)
            #                     print(fact)
            for i in self.userList:
                if i.id == pid:
                    return [True, count, statement, i.cards]
        else:
            result = '查无此人' + str(pid) + '不许出牌'
            print('查无此人', pid, '不许出牌')
            return result

    def handle_challenge(self, pid):
        status = False
        result = ""
        for i in range(len(self.userList)):
            if self.userList[i].id == pid and pid != self.lastPlayerId:
                status = True
        if status and self.lastPlayerId != None:
            if self.isTrue is True:
                id = pid
            else:
                id = self.lastPlayerId
            for i in range(len(self.userList)):
                if self.userList[i].id == id:
                    print('本轮', self.userList[i].name, '负，桌上牌全归他')
                    while len(cards) > 0:
                        self.userList[i].cards.append(cards.pop())
                    result = ['本轮:' + self.userList[i].name + ':负，桌上牌全归他',self.userList[i].cards,self.userList[i].id]
            self.lastPlayerId = None
            self.generate_next_id(pid)
        else:
            result = '质疑者身份非法，质疑无效'
            print('质疑者身份非法，质疑无效')
        return result

    def handle_pass(self, pid):
        status = False
        name = ""
        for i in range(len(self.userList)):
            if self.userList[i].id == pid and pid != self.lastPlayerId:
                status = True
                name = self.userList[i].name
        if status is True:
            self.passAmount = self.passAmount + 1
            print(name, '选择Pass')
            if self.passAmount >= len(self.userList) - 1:
                print("所有用户均选择Pass，桌上的牌将清空")
                while len(cards) > 0:
                    cards.pop()
            self.generate_next_id(pid)
        else:
            print('不允许当前用户Pass')

    def deal(self):
        self.shuffle()
        onePlayerAmount = math.ceil(len(cards) / self.leftAmount)
        while len(cards) > 0 and len(cards) < onePlayerAmount:
            onePlayerAmount = onePlayerAmount - 1
        temp_cards = []
        for i in range(onePlayerAmount):
            temp_cards.append(cards.pop())
        self.leftAmount = self.leftAmount - 1
        return temp_cards

    def canIPlay(self, nextId):
        if nextId == self.nextId:
            return True
        return False

    def showCards(self):
        print('荷官手中的余牌:')
        print(cards)


def get_card_type(str):
    return str[0]


def word_topic(str):
    get_type = get_card_type(str)


if __name__ == '__main__':
    a = SexyDealer(4)
